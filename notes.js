const fs = require('fs')
const chalk = require('chalk')

const addNote = (title, body) => {
    const notes = loadNotes()
    
    //const dyplicateNotes = notes.filter((note) => notes.title === title)
    const dyplicateNote = notes.find((note) => note.title === title)
    // const dyplicateNotes = notes.filter(function(notes)
    // {
    //     return notes.title === title
    // })

    if(!dyplicateNote)
    {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log('New note added!')
    }
    else
    {
        console.log('Note title taken!')
    }
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json',dataJSON)

}

const loadNotes = () => {
    try
    {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)

    }
    catch(e)
    {
        return[]
    }
}

const removeNote = (title) => {
    const notes = loadNotes()
    const notesToKeep = notes.filter((note) => note.title !== title)
    // const notesToKeep = notes.filter(function (note) {
    //     return note.title !== title
    // })

    if (notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse('Note removed!'))
        saveNotes(notesToKeep)
    } else {
        console.log(chalk.red.inverse('No note found!'))
    }    
}

const listNotes = () => {
    const notes = loadNotes()
    console.log(chalk.inverse('Your notes'))
    
    notes.forEach((note) => {
        console.log(note.title)
    })
    note.forEach
}

const readNote = (title) => {
    const notes = loadNotes()
    const Note = notes.find((note) => note.title === title)

    if(!Note){
        console.log(chalk.red('Note no found'))
    }
    else{
        console.log(chalk.green(Note.title))
        console.log(Note.body)
    }
        
}


module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}